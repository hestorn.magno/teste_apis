#language: pt

Funcionalidade:
  Validar alteração de uma pessoa via API.


  Esquema do Cenário: Alterar pessoa com o ID pelo nome da lista
    Dado o endpoint da API PUT
    Quando eu enviar uma requisição PUT para o "<ID>" com "<nomeAlterado>", "<sobrenomeAlterado>", "<emailAlterado>"
    Entao a API ira retornar o codigo 200
    E os dados da request enviada com o "<nomeAlterado>" alterado
    Exemplos:

      |ID  |   |nomeAlterado | |sobrenomeAlterado | |emailAlterado         |
      |20  |   |LucasAlt     | |SobrenomeAlt1     | |emailalt1@nao.tem     |
