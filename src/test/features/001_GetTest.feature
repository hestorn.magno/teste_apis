#language: pt

Funcionalidade:
  Validar diferentes operações GET na API.

  Cenario: Consultar pessoas
    Dado o endpoint da API GET
    Quando realizar uma requisição GET
    Entao a API irá retornar o codigo 200
    E a lista de pessoas igual o schema

  Esquema do Cenário: Consultar uma pessoa por nome
    Dado o endpoint da API GET
    Quando realizar uma requisição GET passando o "<nome>"
    Entao a API irá retornar o codigo 200
    E os dados da pessoa com nome "<nome>"
    Exemplos:
    |nome|
    |LucasAlterado|

