#language: pt

Funcionalidade:
  Validar inclusão de pessoa através do método POST.


  Esquema do Cenário: Incluir pessoas da lista
    Dado o endpoint da API POST
    Quando eu enviar uma requisição POST com "<nome>", "<sobrenome>", "<email>"
    Entao a API ira retornar o codigo 201
    E os dados da request enviada com o "<nome>"
    Exemplos:

    |nome  |   |sobrenome |   |email            |
    |Lucas |   |Magno     |   |teste@email.com  |
