#language: pt

Funcionalidade:
  Validar a exclusão de pessoas na API.

  Esquema do Cenário: Excluir uma pessoa
    Dado o endpoint da API DELETE
    Quando eu enviar uma requisição DELETE com a "<ID>" da pessoa
    Entao a API ira retornar o codigo <200>
    Exemplos:
      | ID  |
      | 20  |

  Esquema do Cenário: Tentar excluir pessoa que não existe
    Dado o endpoint da API DELETE
    Quando eu enviar uma requisição DELETE com a "<ID>" de uma pessoa que não existe
    Entao a API ira retornar o codigo <404>
    Exemplos:

    |ID|
    |999|