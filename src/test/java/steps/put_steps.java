package steps;

import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.jupiter.api.Assertions;

/**
 * Passos para validar o método PUT na API
 */
public class put_steps {
    private static Response response;
    private static RequestSpecification httpRequest;
    private String url;

    @Dado("o endpoint da API PUT")
    public void oEndpointDaAPIPUT() {
        url = "http://localhost:3000/pessoas/";
    }

    @Quando("eu enviar uma requisição PUT para o {string} com {string}, {string}, {string}")
    public void euEnviarUmaRequisiçãoPUTParaOCom(String id, String nomeAlt, String sobrenomeAlt, String emailAlt) {
        String myJson =
                "{\"nome\":\""+nomeAlt+"\"," +
                "\"sobrenome\": \""+sobrenomeAlt+"\", " +
                "\"email\": \""+emailAlt+"\"" +
                "}";
        httpRequest = RestAssured.given().contentType("application/json").body(myJson);
        response = httpRequest.put(url+id);
    }

    @Entao("a API ira retornar o codigo 200")
    public void aAPIIraRetornarOCodigo200() {
        Assertions.assertEquals(response.getStatusCode(), 200);
    }

    @E("os dados da request enviada com o {string} alterado")
    public void osDadosDaRequestEnviadaComOAlterado(String nomeAlt) {
        Assertions.assertEquals(response.jsonPath().get("nome"), nomeAlt);
    }
}
