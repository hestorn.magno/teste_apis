package steps;

import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.jupiter.api.Assertions;


/**
 * Passos para validar o método DELETE na API
 */
public class delete_steps {

    private static Response response;
    private static RequestSpecification httpRequest;
    private String url;


    @Dado("o endpoint da API DELETE")
    public void oEndpointDaAPIDELETE() {
        url = "http://localhost:3000/pessoas/";
    }


    @Quando("eu enviar uma requisição DELETE com a {string} da pessoa")
    public void euEnviarUmaRequisiçãoDELETEComADaPessoa(String id) {
        httpRequest = RestAssured.given();
        response = httpRequest.delete(url + id);
    }


    @Entao("a API ira retornar o codigo <{int}>")
    public void aAPIIraRetornarOCodigo(int cod) {
        Assertions.assertEquals(response.getStatusCode(), cod);
    }

    @Quando("eu enviar uma requisição DELETE com a {string} de uma pessoa que não existe")
    public void euEnviarUmaRequisiçãoDELETEComADeUmaPessoaQueNãoExiste(String id) {

        httpRequest = RestAssured.given();
        response = httpRequest.delete(url + id);

    }
}