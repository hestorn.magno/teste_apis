package steps;

import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.jupiter.api.Assertions;


/**
 * Passos para validar o método POST na API
 */
public class post_steps {
    private static Response response;
    private static RequestSpecification httpRequest;
    private String url;


    @Dado("o endpoint da API POST")
    public void oEndpointDaAPIPost() {
        url = "http://localhost:3000/pessoas";
    }

    @Entao("a API ira retornar o codigo 201")
    public void aAPIIraRetornarOCodigo() {
        Assertions.assertEquals(response.getStatusCode(), 201);
    }

    @Quando("eu enviar uma requisição POST com {string}, {string}, {string}")
    public void euEnviarUmaRequisiçãoPOSTCom(String nome, String sobrenome, String email) {
        String myJson = "{\"nome\":\""+nome+"\",\"sobrenome\": \""+sobrenome+"\", \"email\": \""+email+"\"}";
        httpRequest = RestAssured.given().contentType("application/json").body(myJson);
        response = httpRequest.post(url);
    }

    @E("os dados da request enviada com o {string}")
    public void osDadosDaRequestEnviadaComO(String nome) {
        Assertions.assertEquals(response.jsonPath().get("nome"), nome);
    }
}
