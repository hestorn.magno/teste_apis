package steps;

import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.module.jsv.JsonSchemaValidator;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.jupiter.api.Assertions;


/**
 * Passos para validar o método GET na API
 */
public class get_steps {

    private static Response response;
    private static RequestSpecification httpRequest;
    private String url;

    @Dado("o endpoint da API GET")
    public void oEndpointDaAPI() {
        url = "http://localhost:3000";
    }

    @Quando("realizar uma requisição GET")
    public void realizarUmaRequisicaoGET() {
        RestAssured.baseURI = url;
        httpRequest = RestAssured.given();
        response = httpRequest.request(Method.GET, "/pessoas");
    }

    @Entao("a API irá retornar o codigo 200")
    public void aAPIIraRetornarOCodigo() {
        Assertions.assertEquals(response.getStatusCode(), 200);
    }

    @E("a lista de pessoas igual o schema")
    public void aListaDePessoas() {
        response.then().assertThat().body(JsonSchemaValidator.matchesJsonSchemaInClasspath("schema_getUsers.json"));
    }

    @Quando("realizar uma requisição GET passando o {string}")
    public void realizarUmaRequisiçãoGETPassandoO(String nome) {
            RestAssured.baseURI = url;
            httpRequest = RestAssured.given();
            response = httpRequest.request(Method.GET, "/pessoas?nome="+nome+"");
        }

    @E("os dados da pessoa com nome {string}")
    public void osDadosDaPessoaComNome(String nomeValid) {
        String nomeFormatado = "["+nomeValid+"]";
        Assertions.assertEquals(response.jsonPath().get("nome").toString(), nomeFormatado);
    }
}

