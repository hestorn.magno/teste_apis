## Testes API

Esse projeto realiza a implementação de uma API para testes utilizando json-server e métodos de
validação nos endpoints criados utilizando Java, JUnit/TestNG e RestAssured.
Também é gerado um relatório Web com os dados obtidos no teste, utilizando o Allure.
E por fim, o projeto roda em pipelines Jenkins utilizando o Jenkinsfile.

## Technology

Tecnologias utilizadas nesse projeto:

* Java version 1.8.0_271
* JUnit 5
* RestAssured 4.3.1
* Allure 2.13.6
* Node.js version 14.15.0 x64



## Getting started

* Json-server
>     É o programa que cria nossa API para testes, para isso, é necessário ter o npm funcionando(pode ser obtido
>     baixando o node.js).
>     Com ele instalado, abra o PowerShell e digite: "npm install -g json-server"
>     Após, navegue até o local onde baixou esse cógido e digite: "json-server db.json" -> Isso irá
>     iniciar nossa API no endereço "localhost:3000" utilizando os dados do arquivo "db.json"

* Allure
>     O Allure é um framework para gerar relatórios em html utilizando os dados dos testes.
>     Para instalar, precisaremos do "scoop" antes, para isso, abra o PowerShell e digite
>     "iwr -useb get.scoop.sh | iex", com ele instalado, basta digitar "scoop install allure".

* Projeto
>     O programa é um projeto Maven feito no IntelliJ com Java.
>     Faça o clone do código, configure o Maven se necessário e digite "mvn compile", isso deverá baixar todas
>     as dependencias não instaladas ainda.

* Jenkinsfile
>Caso deseje incluir o projeto em uma pipeline Jenkins, basta importar o arquivo Jenkinsfile
na pipeline.

## How to use

Com as dependências baixadas e as configurações adicionais feitas, basta inicializar o json-server
(que vai por default na porta :3000):

Para iniciar o json-server: No PowerShell vá até o local que está o db.json
e digite -> "json-server --watch db.json".
Podemos verificar se iniciou em "http://localhost:3000/pessoas".

Após isso, execute os testes(arquivo TestInitialize na pasta Steps) - Ou digite
"mvn clean install" no terminal.

Após a execução, digite no powershell: allure serve C:/*CAMINHO PRO REPOSITORIO*/Desafio/target/surefire-reports/
Isso irá gerar o relatório com os dados do último teste e exibirá no navegador.



## Versioning

1.0.0.0


## Authors

* **Lucas Magno S. Teichmann**: @hestorn.magno(https://gitlab.com/hestorn.magno/)




